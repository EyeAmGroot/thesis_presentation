<!DOCTYPE html>
<html lang="en">
<meta name="robots" content="noindex">
<head>
	<title>Design of bit-flipping decoding techniques for high data-rate coded modulation</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Styles -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="src/resources/css/styles.css">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
	<!-- scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>
	<script type="text/x-mathjax-config">MathJax.Hub.Config({ TeX: { extensions: ["color.js"] }});</script>

</head>
<body>
<div class="jumbotron text-center">
	<h1>Design of bit-flipping decoding techniques for high data-rate coded modulation</h1>
	<h2>Dominic Sherry</h2>
	<h4>Supervisor: Dr. Mark Flanagan</h4>
	<h6 style="color: gray;">UCD School of Electrical, Electronic and Communications Engineering</h6>
	<img class="float-right" style="max-width: 10%;" src="src/resources/images/ucd.gif"/>
</div>

<!-- Define mathjax colours -->

$$
\definecolor{message}{RGB}{43,109,169}
\definecolor{parity}{RGB}{0,145,65}
\definecolor{syndrome}{RGB}{192,0,0}
\definecolor{error}{RGB}{192,0,0}
\definecolor{new}{RGB}{192,0,0}
\definecolor{black}{RGB}{0,0,0}
$$

<div class="container text-justify">
	<div class="row">
		<div class="col-sm-6">
			<h3>Abstract</h3>
			<p>
				In this project I aim to implement some bit-flipping decoding algorithms which work over Binary Phase Shift Keying (BPSK) over more complex decoding schemes. This means the throughput of data can be greatly increased while still employing excellent error correction at the receiver.
			</p>
			<h3>Why is error correction useful?</h3>
				<ul class="list">
					<li>Better error correction leads to better performance at low SNRs.</li>
					<li>If we can correct errors at the receiver, we don’t need to retransmit packets.</li>
					<ul><li>This increases reliability, and helps reduce jitter/latency.</li></ul>
					<li>Often tranmsitted messages are completely useless if a single bit is received incorrectly.</li>
					<ul><li>Examples include encrypted messages, spreadsheets, documents, banking information.</li></ul>
				</ul>

			<h3>Error Correction using Parity Checking</h3>
				<ul class="list">
					<li>Example: Want to transmit the binary message <kbd>101</kbd>.</li>
				</ul>

				<h4>Single Parity Check</h4>

				<ul class="list">
					<li>Add redundant parity-check bit \( C_4 \).</li>
					$$ C_1 \oplus C_2 \oplus C_3 \oplus C_4 = 0 $$
					<li>Transmit codeword <kbd>1010</kbd>.</li>
					<li>Can only detect if an odd number of errors occurs.</li>
				</ul>

				<h4>Multiple Parity Checks</h4>

				<ul class="list">
					<li>Codeword \( \begin{bmatrix}\color{message} C_1 & \color{message} C_2 & \color{message} C_3 & \color{parity} C_4 & \color{parity} C_5 & \color{parity} C_6\end{bmatrix} \)</li>
					<li>Message bits in blue, parity-check bits in green.</li>
					<li>Parity-check constraints:</li>

					$$\color{message}C_1 \color{black}\oplus \color{message}C_2 \color{black}\oplus \color{parity}C_4 \color{black}= 0$$

					$$\color{message}C_2 \color{black}\oplus \color{message}C_3 \color{black}\oplus \color{parity}C_5 \color{black}= 0$$

					$$\color{message}C_1 \color{black}\oplus \color{parity}C_5 \color{black}\oplus \color{parity}C_6 \color{black}= 0$$

					$$\color{message}C_3 \color{black}\oplus \color{parity}C_4 \color{black}\oplus \color{parity}C_6 \color{black}= 0$$

					<li>Transmit codeword <kbd>101110</kbd>, receive <kbd>100110</kbd>.</li>

					$$\color{message}1 \color{black}\oplus \color{message}0 \color{black}\oplus \color{parity}1 \color{black}= 0$$

					$$\color{message}0 \color{black}\oplus \color{message}0 \color{black}\oplus \color{parity}1 \color{black}= \color{error}1$$

					$$\color{message}1 \color{black}\oplus \color{parity}1 \color{black}\oplus \color{parity}0 \color{black}= 0$$

					$$\color{message}0 \color{black}\oplus \color{parity}1 \color{black}\oplus \color{parity}0 \color{black}= \color{error}1$$

					<li>Some parity check equations not satisfied, <kbd>101010</kbd> is not a valid codeword.</li>
					<li>The vector of parity-check results <kbd>0101</kbd> is called the <mark>syndrome</mark>.</li>
				</ul>

			<h3>Tanner Graphs</h3>
			<ul class="list">
				<li>A useful way of visualising systems of parity-check equations.</li>
			</ul>

			<img class="img-fluid" src="src/resources/images/tanner/tanner_error.png"/>

			<ul class="list">
				<li>Codeword bits circled, syndrome bits boxed in red.</li>
				<li>Parity-check equations represented through <mark>Tanner graph edges</mark>.</li>
			</ul>

			<h3>LDPC (Low Density Parity-Check) Codes</h3>
			<ul class="list">
				<li>LDPC codes have excellent performance and are commonly used in modern communication systems.</li>
				<li>Invented by Robert G. Gallager in 1962. [1]</li>
				<li>Each parity check covers a small number of codeword bits.</li>
				<li>Each parity check covers bits also covered by many other parity checks.</li>
				<li>Examples include:
					<ul>
						<li>IEEE 802.3an standard for 10 Gbit Ethernet</li>
						<li>IEEE 802.11n standard for WiFi</li>
						<li>IEEE 802.16e standard for microwave communications (WiMAX)</li>
					</ul>
			</ul>

			<h3>Decoding over BPSK (Binary Phase Shift Keying)</h3>
			<ul class="list">
				<li>Example: Want to transmit the binary message <kbd>001</kbd>.</li>
				<li>Transmit codeword \( \begin{bmatrix}\color{message} 0 & \color{message} 0 & \color{message} 1 & \color{parity} 0 & \color{parity} 1 & \color{parity} 1\end{bmatrix} \) over BPSK with symbol energy \( E_S = 1 \).</li>
				
				$$ X = \begin{bmatrix} -1 & -1 & 1 & -1 & 1 & 1\end{bmatrix} $$

				<li>A noisy version of the transmitted signal is received:</li>

				$$ y = \begin{bmatrix} -0.9 & -1.5 & 0.5 & \color{error}0.1 & 0.8 & 0.7\end{bmatrix} $$

				<li>The noisy signal is incorrectly interpreted as</li>
				
				$$ Z = \begin{bmatrix} 0 & 0 & 1 & \color{error}1 & 1 & 1\end{bmatrix} $$

				<li>Use iterative decoding algorithms to correct errors in \( Z \).</li>
			</ul>

				<h4>BF (Bit-Flipping) Decoding</h4>
				<ul class="list">
					<li>Flip any bits in the codeword where the majority of connected syndrome bits are nonzero.</li>
				</ul>
				<img class="img-fluid" src="src/resources/images/tanner/bf3.png"/>
		</div>
		
		<div class=col-sm-6>
				<h4>WBF (Weighted Bit-Flipping) Decoding</h4>
				<p>The magnitude of each received symbol is taken as a measure of its reliability.</p>

				<ol>
					<li>
						Get the syndrome of \( Z \) using parity check equations. If all the syndrome bits are zeros, take \( Z \) to be the correct codeword \( y \).
					</li>
					<li>
						\( Y \) measures the reliability of the parity-check equations using the syndrome and the amplitudes of the received symbols.

						$$ Y_m=(2S_m-1)\min_{n∈N(m)}{|y_n |} $$
					</li>
					<li>
						\( E \) is a sequence which measures the reliability of the received symbols in \( Y \).

						$$ E_n = \sum_{m \in M(n)}{Y_m} $$
					</li>
					<li>
						Flip the bit in \( Z \) at the position corresponding to the most positive entry in \( E \).
					</li>
				</ol>
					<img class="img-fluid" src="src/resources/images/tanner/wbf7.png"/>

				<h4>MWBF (Modified WBF) Decoding [5]</h4>
					<p>
						Modify how \( E \) is calculated to also take the reliabilities of the individual received symbols into account.

						$$ E_n = \sum_{m \in M(n)}{Y_m} \color{new}- \alpha|y_n|\color{black}, \quad \alpha \geq 0 $$
					</p>
				<img class="img-fluid" src="src/resources/images/plots/bpsk.svg"/>

				<h3>Decoding over PAM (Pulse Amplitude Modulation) / QAM (Quadrature Amplitude Modulation)</h3>
				<ul class="list">
					<li>When decoding with BPSK each symbol represents one bit.</li>
					<ul><li>Not the case for higher order modulation.</li></ul>
					<li>Need to find a way to get the bit reliabilities from complex symbols.</li>
				</ul>

				<h3>An implementation of MWBF decoding for N-PAM Modulation</h3>
				<p>In this example I am using MWBF decoding over 4-PAM.</p>

				<img style="display: block; height: 300px; margin-left: auto; margin-right: auto;" src="src/resources/images/constellation/4pam.png"/>

				<ul class="list">
					<li>Want to transmit codeword \( \begin{bmatrix}\color{message} 0 & \color{message} 0 & \color{message} 1 & \color{parity} 0 & \color{parity} 1 & \color{parity} 1\end{bmatrix} \) over 4-PAM.</li>
					
					$$ X = \begin{bmatrix} -1.5d & +1.5d & +0.5d\end{bmatrix} $$

					<li>A noisy version of the transmitted signal is received:</li>

					$$ y = \begin{bmatrix} -1.6d & +0.9d & +0.2d \end{bmatrix} $$

					<li>The noisy signal is incorrectly interpreted as</li>
					
					$$ Z = \begin{bmatrix} \begin{bmatrix} 0 & 0\end{bmatrix} & \begin{bmatrix} 1 & \color{error}1\end{bmatrix} & \begin{bmatrix} 1 & 1\end{bmatrix}\end{bmatrix} $$

					<li>Need a function to approximate the reliability of each bit in \( Z \) before implementing MWBF decoding:</li>

					$$
					f_{l,j} = \sum_{i=1}^{L}\frac{h_{i,j}}{(1+\mid l_i-y_l \mid)^2}, \quad
					h_{i,j}=\begin{cases}
					1, & \text{if } g_{i,j}=z_{l,j} \\
					-1, & \text{otherwise}
					\end{cases}
					$$

					<li>
						Essentially rewarding received bits which have the same polarity as bits in possible transmit symbols and penalising received bits which have opposite polarity.
					</li>
					<li>
						Received symbols which are closer to possible transmit symbols get rewarded or penalised more heavily than ones which are further away.
					</li>
					<li>
						Can now implement decoding algorithms such as MWBF by replacing \( \mid y_n \mid \) with \( f_n \) in calculating \( Y \) and \( E \).
					</li>
					<li>
						Since I and Q components of a QAM symbol can be represented by the least significant / most significant bits of a gray-coded even-PAM symbol, implementing similar decoding algorithms over QAM is trivial.
					</li>
				</ul>

			<img class="img-fluid" src="src/resources/images/plots/4pam.svg"/>

			<h3>Future Work</h3>
			<ul class="list">
				<li>Implement decoding over higher order modulation schemes: 8-PAM / 64-QAM</li>
				<li>Implement PWBF over PAM / QAM schemes for faster decoding.</li>
				<li>Improvements to PAM MWBF algorithm: Reduce complexity of decoding at the receiver by not accounting for the likelihood of transmission of symbols beyond a given distance from the received symbol.</li>
				<li>Decoder-demapper feedback loop.</li>
			</ul>
		</div>
	</div>
	<div class="row text-justify">
		<div class="col">
			<h3>References</h3>
			<p>[1] Gallager, R. 1962, "Low-density parity-check codes", IRE Transactions on Information Theory, vol. 8, no. 1, pp. 21-28.</p>
			<p>[2] Shannon, C.E. & Weaver, W. 1949, The mathematical theory of communication, University of Illinois Press, Urbana [Ill.];London;.</p>
			<p>[3] https://medium.com/5g-nr/ldpc-low-density-parity-check-code-8a4444153934</p>
			<p>[4]	Johnson, S.J., 2010. Iterative error correction: Turbo, low-density parity-check and repeat-accumulate codes. Cambridge university press.</p>
			<p>[5]	Zhang, J. and Fossorier, M.P., 2004. A modified weighted bit-flipping decoding of low-density parity-check codes. IEEE Communications Letters, 8(3), pp.165-167.</p>
			<p>[6]	Wu, X., Zhao, C. and You, X., 2007. Parallel weighted bit-flipping decoding. IEEE Communications letters, 11(8).</p>
		</div>
	</div>
</div>

</body>
</html>